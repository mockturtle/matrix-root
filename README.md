# matrix root

A simple Ada generic package  to compute the N-th root of a matrix. At the moment it works only with 2x2 matrices since it is the case I need now.  Maybe later I will extend it to larger matrices.