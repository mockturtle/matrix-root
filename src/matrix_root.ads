with Ada.Numerics.Generic_Real_Arrays;

generic
   type Real is digits <>;

   with package Matrix_Package is
     new Ada.Numerics.Generic_Real_Arrays (Real);
package Matrix_Root is
   use type Matrix_Package.Real_Matrix;
   use type Matrix_Package.Real_Vector;

   function Is_Square (M : Matrix_Package.Real_Matrix) return Boolean
   is (M'Length (1) = M'Length (2));
   -- Useful in contracts

   type Affine_Transformation (<>) is private;

   function Create (Linear      : Matrix_Package.Real_Matrix;
                    Translation : Matrix_Package.Real_Vector)
                    return Affine_Transformation
     with
       Pre =>
         Is_Square (Linear)
     and Linear'Length (1) = Translation'Length,
     Post =>
       Order (Create'Result) = Translation'Length;

   function Order (T : Affine_Transformation) return Positive;

   function "*" (X, Y : Affine_Transformation) return Affine_Transformation;

   function Identity (Order : Positive) return Affine_Transformation;

   function Is_Invertible (X : Affine_Transformation) return Boolean;

   function Inverse (X : Affine_Transformation) return Affine_Transformation;

   type Norm_Kind is (One, Two, Frobenius, Infinity);

   function Norm (M    : Matrix_Package.Real_Matrix;
                  Kind : Norm_Kind)
                  return Real
     with
       Pre =>
         Is_Square (M)
     and Kind /= Two, -- Still unimplemented

     Post =>
       Norm'Result >= 0.0;

   function Is_Numerically_Zero (M   : Matrix_Package.Real_Matrix;
                                 Tol : Real := 1.0e-9)
                                    return Boolean
   is (Norm (M, Infinity) <= Tol);



   function "**" (M : Matrix_Package.Real_Matrix; Exponent : Natural)
                  return Matrix_Package.Real_Matrix
     with
       Pre =>
         Is_Square (M);

   function Nth_Root (M : Matrix_Package.Real_Matrix;
                      N : Positive)
                      return Matrix_Package.Real_Matrix
     with
       Pre =>
         Is_Square (M)
     and Matrix_Package.Determinant (M) > 0.0
     and M'Length (2) = 2,  -- Larger cases still unimplemented

     Post =>
       Is_Numerically_Zero (Nth_Root'Result ** N - M);

   function Nth_Root (M : Affine_Transformation;
                      N : Positive)
                      return Affine_Transformation;

private

   type Affine_Transformation (Order : Positive) is
      record
         Linear      : Matrix_Package.Real_Matrix (1 .. Order, 1 .. Order);
         Translation : Matrix_Package.Real_Vector (1 .. Order);
      end record;

   function Create (Linear      : Matrix_Package.Real_Matrix;
                    Translation : Matrix_Package.Real_Vector)
                    return Affine_Transformation
   is (if
         Linear'Length (1) = Linear'Length (2) and
         Linear'Length (1) = Translation'Length
       then
          Affine_Transformation'(Order       => Translation'Length,
                                 Linear      => Linear,
                                 Translation => Translation)

       else
          raise Constraint_Error);

   function Order (T : Affine_Transformation) return Positive
   is (T.Order);


   function "*" (X, Y : Affine_Transformation) return Affine_Transformation
   is (if Order (X) = Order (Y) then
          Create (Linear      => X.Linear * Y.Linear,
                  Translation => X.Linear * Y.Translation + X.Translation)

       else
          raise Constraint_Error);


   function Identity (Order : Positive) return Affine_Transformation
   is (Affine_Transformation'(Order       => Order,
                              Linear      => Matrix_Package.Unit_Matrix (Order),
                              Translation => (others => 0.0)));


   function Is_Invertible (X : Affine_Transformation) return Boolean
   is (abs (Matrix_Package.Determinant (X.Linear)) > 1.0e-9);

   function Inverse (X : Affine_Transformation) return Affine_Transformation
   is (Create (Linear      => Matrix_Package.Inverse (X.Linear),
               Translation => -Matrix_Package.Inverse (X.Linear) * X.Translation));

end Matrix_Root;
