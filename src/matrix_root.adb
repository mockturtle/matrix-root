pragma Ada_2012;
with Ada.Numerics.Generic_Elementary_Functions;

package body Matrix_Root is
   package Elementary_Functions is
     new Ada.Numerics.Generic_Elementary_Functions (Real);

   use Elementary_Functions;
   use Matrix_Package;

   function Rotation (Angle : Real) return Real_Matrix
   is (1 => (1 => Cos (Angle), 2 => -Sin (Angle)),
       2 => (1 => Sin (Angle), 2 => Cos (Angle)));

   ----------
   -- Norm --
   ----------

   function Norm (M : Matrix_Package.Real_Matrix; Kind : Norm_Kind) return Real
   is
      use Matrix_Package;

   begin
      case Kind is
         when One =>
            declare
               Result : Real := 0.0;
               Accum  : Real;
            begin
               for Col in M'Range (2) loop
                  Accum := 0.0;

                  for Row in M'Range (1) loop
                     Accum := Accum + abs M (Row, Col);
                  end loop;

                  Result := Real'Max (Result, Accum);
               end loop;

               return Result;
            end;

         when Two =>
            raise Constraint_Error with "2-norm still unimplemented";

         when Infinity =>
            return Norm (Transpose (M), One);

         when Frobenius =>
            declare
               Result : Real := 0.0;
            begin
               for Col in M'Range (2) loop
                  for Row in M'Range (1) loop
                     Result := Result + M (Row, Col) ** 2;
                  end loop;
               end loop;

               return Sqrt (Result);
            end;
      end case;
   end Norm;

   ----------
   -- "**" --
   ----------

   function "**" (M        : Matrix_Package.Real_Matrix;
                  Exponent : Natural)
                  return Matrix_Package.Real_Matrix
   is
      use Matrix_Package;

   begin
      if M'Length (1) /= M'Length (2) then
         raise Constraint_Error;
      end if;

      declare
         Result : Real_Matrix := Unit_Matrix (M'Length);
         Status : Real_Matrix := M;
         N      : Natural := Exponent;
      begin
         while N > 0 loop
            if N mod 2 = 1 then
               Result := Result * Status;
            end if;

            Status := Status * Status;
            N := N / 2;
         end loop;

         return Result;
      end;
   end "**";

   --------------
   -- Nth_Root --
   --------------

   function Nth_Root
     (M : Matrix_Package.Real_Matrix; N : Positive)
      return Matrix_Package.Real_Matrix
   is
      use Matrix_Package;
      --
      -- It is worth saying two words about the algorithm used
      -- to compute the nth root of a matrix, since it is not trivial.
      --
      -- We consider two cases: when M is a rotation matrix with a scale
      -- factor and the case of generic M with det(M)>0.
      -- The former case needs a special treatment
      --
      -- In the first case we decompose M as M = S*R where S is a real number
      -- and R is a "pure rotation" (that is, det(R)=1 and R'*R=I).  In this
      -- case the nth root is S**(1/N) * R**(1/N).  We consider only the case
      -- 2x2 therefore, if Theta is the rotation angle of R, Theta/N is the
      -- rotation angle of R**(1/N).
      --
      -- In the generic case we decompose M as
      --
      --     M = R * T * R'
      --
      -- where R is a rotation and T an upper triangular matrix.  The nth
      -- root of M is
      --
      --        R * (T ** (1/N)) * R'
      --
      -- that we can compute as soon as we know how to compute the nth root
      -- of an upper triangular matrix
      --
      -- At the moment only the case 2x2 is implemented using some ad-hoc
      -- algorithms
      --
      procedure Check_If_Scaled_Rotation
        (Item          : Real_Matrix;
         Is_Rotation   : out Boolean;
         Scale         : out Real;
         Pure_Rotation : out Real_Matrix)
        with
          Pre => Is_Square (Item);

      procedure Check_If_Scaled_Rotation
        (Item          : Real_Matrix;
         Is_Rotation   : out Boolean;
         Scale         : out Real;
         Pure_Rotation : out Real_Matrix)
      is
         procedure Check_2x2_Matrix is
         begin
            if abs (Item (1, 1)-Item (2, 2))+ abs (Item (1, 2)+Item (2, 1)) <= 1.0e-9 then
               Scale := Sqrt (Item (1, 1) ** 2 + Item (2, 2) ** 2);
               Is_Rotation := True;
            else
               Is_Rotation := False;
            end if;
         end Check_2x2_Matrix;

         ----------------------
         -- Is_Scalar_Matrix --
         ----------------------

         function Is_Scalar_Matrix (X : Real_Matrix) return Boolean
         is
            Diag : Real_Matrix (X'Range (1), X'Range (2)) :=
                     (others => (others => 0.0));
         begin
            for K in Diag'Range (1) loop
               Diag (K, K) := X (1, 1);
            end loop;

            return Norm (X - Diag, Infinity) <= 1.0e-6;
         end Is_Scalar_Matrix;

      begin
         if Item'Length (1) = 2 then
            Check_2x2_Matrix;
         else
            declare
               Tmp  : constant Real_Matrix := Item * Transpose (Item);
            begin
               if Is_Scalar_Matrix (Tmp)  then
                  Scale := Sqrt (Tmp (1, 1));
                  Is_Rotation := True;
               else
                  Is_Rotation := False;
               end if;
            end;
         end if;

         if Is_Rotation then
            Pure_Rotation := (1.0 / Scale) * Item;
         else
            Pure_Rotation := (others => (others => 0.0));
            Scale := 0.0;
         end if;
      end Check_If_Scaled_Rotation;

      function Nth_Root_Of_Rotation (Item : Real_Matrix;
                                     N    : Positive)
                                     return Real_Matrix
        with
          Pre => Is_Square (Item);

      function Nth_Root_Of_Rotation (Item : Real_Matrix;
                                     N    : Positive) return Real_Matrix
      is
         Angle : Real;
      begin
         if not Is_Square (Item) then
            raise Program_Error;
         end if;

         if Item'Length (1) /= 2 then
            raise Program_Error with "Nth Root of rotation /= 2x2 not implemented";
         end if;

         Angle := Arctan (Y => Item (2, 1),
                          X => Item (1, 1)) / Real (N);

         return Rotation (Angle);

      end Nth_Root_Of_Rotation;

      function Is_Upper (X : Real_Matrix) return Boolean
      is
      begin
         for Row in X'Range (1) loop
            for Col in X'First (2) .. X'First (2)+Row - 2 loop
               if abs X (Row, Col) > 1.0e-9 then
                  return False;
               end if;
            end loop;
         end loop;

         return True;
      end Is_Upper;

      function Nth_Root_Of_Triangular (X : Real_Matrix; N : Positive)
                                       return Real_Matrix
        with
          Pre => Is_Square (X) and Is_Upper (X);

      function Nth_Root_Of_Triangular (X : Real_Matrix; N : Positive)
                                       return Real_Matrix
      is
         function Case_2x2 (X : Real_Matrix; N : Positive) return Real_Matrix
         is
            --
            -- Let A be the triangular matrix [a, b; 0, d].  Its nth
            -- power is
            --
            --   [a**n,  b*u_n; 0, d**n]
            --
            -- where
            --
            --    u_n(a,d) = a**n + a**(n-1) ** d + a**(n-2) * d**2 + ... + d**n
            --
            -- Therefore, the nth-root of [x, y ; 0 z] has
            --
            --   a = x**1/n
            --   d = z**1_n
            --   b = y / u_n(a, d)
            --
            -- An efficient way of computing u_n follows from the recursive
            -- relation
            --
            --    u_n = a u_{n-1} + d**n
            --    u_0 = 1
            --
            Exp    : constant Real := 1.0 / Real (N);

            D1 : constant Real := X (X'First (1), X'First (2)) ** Exp;
            D2 : constant Real := X (X'First (1)+1, X'First (2)+1) ** Exp;
            C  : constant Real := X (X'First (1), X'First (2)+1);

            Accum  : Real;  -- will contain u_n
            Status : Real;  -- keep d**n
         begin
            Status := D2;

            Accum := 1.0;
            for K in 1 .. N loop
               Accum := D1 * Accum + Status;
               -- here Accum = u_k

               Status := Status * D2;
            end loop;

            return Real_Matrix'(1 => (1 => D1, 2 => C / Accum),
                                2 => (1 => 0.0, 2 => D2));
         end Case_2x2;
      begin
         if not (Is_Square (X) and Is_Upper (X)) then
            raise Program_Error;
         end if;

         if X'Length (1) = 2 then
            return Case_2x2 (X, N);

         else
            raise Program_Error with "Unimplemented";
         end if;
      end Nth_Root_Of_Triangular;

      procedure Make_Triangular (M        : Real_Matrix;
                                 Triang   : out Real_Matrix;
                                 Rotation : out Real_Matrix)
        with Pre =>
          Is_Square (M)
        and Is_Square (Triang)
        and Is_Square (Rotation)
        and M'Length (1) = Triang'Length (1)
        and M'Length (1) = Rotation'Length (1)
        Post =>
          Is_Numerically_Zero (M - Rotation * Triang * Transpose (Rotation));

      procedure Make_Triangular (M        : Real_Matrix;
                                 Triang   : out Real_Matrix;
                                 Rotation : out Real_Matrix)
      is
      begin

      end Make_Triangular;


      Rotation : Real_Matrix (M'Range (1), M'Range (2));
      Scale : Real;
      Is_Rotation : Boolean;
   begin
      if not Is_Square (M) then
         raise Constraint_Error;
      end if;

      Check_If_Scaled_Rotation (Item          => M,
                                Is_Rotation   => Is_Rotation,
                                Scale         => Scale,
                                Pure_Rotation => Rotation);

      if Is_Rotation then
         return (Scale ** (1.0 / Real (N))) * Nth_Root_Of_Rotation (Rotation, N);
      else
         declare
            T : Real_Matrix (M'Range (1), M'Range (2));
         begin
            Make_Triangular (M, Pure_Rotation, T);

            return Rotation * Nth_Root_Of_Triangular (T, N) * Transpose (Rotation);
         end;
      end if;
   end Nth_Root;


   function Nth_Root (M : Affine_Transformation;
                      N : Positive)
                      return Affine_Transformation
   is
      --
      -- Let [A x; 0 1] (in matlab notation) be an affine map. Its
      -- square is
      --
      --    [A**2, A*x+x] (row 0 1 is implicit)
      --
      -- its third power is
      --
      --    [A**3, A**2 * x + A*x + x] = [A**3, (A**2 + A + I) * x]
      --
      -- and its n-th power is
      --
      --    [A**n, Accum_n * x]
      --
      -- where Accum_n = A**(n-1) + A**(n-2) + ... I
      --
      --
      -- Therefore, if M=[C, y] and [A, x] is its nth-root it must be
      --
      --   A = root(C, n)
      --   x = inv(Accum_n) * y
      --
      use Matrix_Package;

      I      : constant Real_Matrix := Unit_Matrix (M.Linear'Length (1));

      Linear : constant Real_Matrix := Nth_Root (M.Linear, N);
      Accum  : Real_Matrix := I;
   begin
      for K in 1 .. N - 1 loop
         Accum := Linear * Accum + I;
      end loop;

      return Create (Linear      => Linear,
                     Translation => Inverse (Accum) * M.Translation);
   end Nth_Root;
end Matrix_Root;
